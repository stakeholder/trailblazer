#!/usr/bin/env bash

rm -f /var/www/tmp/pids/server.pid

echo "Initializing Migration..."
rails db:migrate

echo "Preparing test database..."
rails db:test:prepare


echo "Starting up server"
rails server -b 0.0.0.0 -p 4000
