require 'rails_helper'

RSpec.describe Permission, type: :model do
  context 'when validating associations' do
    it { is_expected.to have_many(:permission_roles) }
    it { is_expected.to have_many(:roles) }
  end
end
