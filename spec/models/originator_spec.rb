require 'rails_helper'

RSpec.describe Originator, type: :model do
  context 'when validating associations' do
    it { is_expected.to have_many(:groups) }
    it { is_expected.to have_many(:users) }
  end
end
