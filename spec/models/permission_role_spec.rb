require 'rails_helper'

RSpec.describe PermissionRole, type: :model do
  context 'when validating associations' do
    it { is_expected.to belong_to(:role) }
    it { is_expected.to belong_to(:permission) }
  end
end
