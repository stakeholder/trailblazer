require 'rails_helper'

RSpec.describe Group, type: :model do
  context 'when validating associations' do
    it { is_expected.to belong_to(:originator) }
    it { is_expected.to have_many(:group_users) }
    it { is_expected.to have_many(:users) }
  end
end
