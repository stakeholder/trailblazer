require 'rails_helper'

RSpec.describe Role, type: :model do
  context 'when validating associations' do
    it { is_expected.to have_many(:users) }
    it { is_expected.to have_many(:permission_roles) }
    it { is_expected.to have_many(:permissions) }
  end
end
