require 'rails_helper'

RSpec.describe UserDetail, type: :model do
  context 'when validating associations' do
    it { is_expected.to belong_to(:user) }
  end
end
