require 'rails_helper'

RSpec.describe User, type: :model do
  context 'when validating associations' do
    it { is_expected.to belong_to(:originator) }
    it { is_expected.to belong_to(:role) }
    it { is_expected.to have_one(:user_detail) }
    it { is_expected.to have_many(:group_users) }
    it { is_expected.to have_many(:groups) }
  end
end
