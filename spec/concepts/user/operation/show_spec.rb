require 'rails_helper'

RSpec.describe User::Operation::Show do
  context 'when a valid id is received' do
    let(:first_user) { FactoryBot.create(:user) }
    let(:second_user) { FactoryBot.create(:user) }
    let(:payload) { { originator_id: second_user.originator_id } }
    let(:params) { { id: second_user.id, payload: payload } }
    let(:result) { User::Operation::Show.call(params) }

    it 'should execute successfully' do
      expect(result).to be_success
    end

    it 'return a user instance' do
      expect(result['model']).to be_a(User)
      expect(result['model'].email).to eq(second_user.email)
    end
  end

  context 'when invalid data is received' do
    let(:result) { User::Operation::Show.call(id: nil) }

    it 'should fail' do
      expect(result.success?).to be false
    end
  end
end
