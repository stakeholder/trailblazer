require 'rails_helper'

RSpec.describe User::Operation::Create do
  before do
    FactoryBot.create(:originator, id: 12, legal_name: 'terminus')
    FactoryBot.create(:role, name: 'Regular')
  end

  context 'when valid parameters are passed' do
    let(:params) do
      FactoryBot.attributes_for(:user).merge(
        originator_id: FactoryBot.create(:originator).id,
        user_detail: FactoryBot.attributes_for(:user_detail)
      )
    end
    let(:payload) { { payload: { originator_id: 12 } } }
    let(:result) { User::Operation::Create.call(params.merge!(payload)) }

    it 'should execute successfully' do
      expect(result).to be_success
    end

    it 'create a new user instance' do
      expect(result['model']).to be_a(User)
    end

    it 'should persist in the database' do
      expect do
        User::Operation::Create.call(params.merge!(payload))
      end.to change(User, :count).by(1)
    end
  end

  context 'when invalid parameters are passed' do
    let(:params) { FactoryBot.attributes_for(:user) }
    let(:payload) { { payload: { originator_id: 12 } } }
    let(:result) { User::Operation::Create.call(params.merge!(payload)) }

    it 'should fail' do
      expect(result).to be_failure
    end

    it 'should not persist in the database' do
      expect do
        User::Operation::Create.call(params.merge!(payload))
      end.not_to change(User, :count)
    end
  end
end
