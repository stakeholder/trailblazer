require 'rails_helper'

RSpec.describe User::Operation::Index do
  before do
    User.delete_all
  end

  context 'when database is empty' do
    let(:result) { User::Operation::Index.call }

    it 'should execute successfully' do
      expect(result).to be_success
    end

    it 'should return an empty array' do
      expect(result['models']).to be_empty
    end
  end

  context 'when database is not empty' do
    let(:user_list) { FactoryBot.create_list(:user, 10) }
    let(:result) { User::Operation::Index.call }

    it 'should execute successfully' do
      expect(result).to be_success
    end

    it 'should return an array with size of 10 ' do
      user_list
      expect(result['models'].count).to eq(10)
    end
  end
end
