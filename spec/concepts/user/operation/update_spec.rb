require 'rails_helper'

RSpec.describe User::Operation::Update do
  context 'when valid parameters are passed' do
    let(:user) { FactoryBot.create(:user) }
    let(:params) { { id: user.id, email: 'test@pinevalley.com' } }
    let(:payload) { { payload: { originator_id: user.originator_id } } }
    let(:result) { User::Operation::Update.call(params.merge!(payload)) }

    it 'should execute successfully' do
      expect(result).to be_success
    end

    it 'updates user in the database' do
      result
      user.reload
      expect(user.email).to eq('test@pinevalley.com')
    end
  end
end

RSpec.describe User::Operation::Update do
  context 'when invalid parameters are passed' do
    let(:user) { FactoryBot.create(:user) }
    let(:params) { { id: user.id, email: '' } }
    let(:result) { User::Operation::Update.call(params) }

    it 'should fail' do
      expect(result).to be_failure
    end

    it 'should not update user in the database' do
      result
      user.reload
      expect(user.email).to_not eq('')
    end
  end
end
