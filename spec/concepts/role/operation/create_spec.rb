require 'rails_helper'

RSpec.describe Role::Operation::Create do
  context 'when valid parameters are passed' do
    let(:params) { FactoryBot.attributes_for(:role, :with_permissions) }
    let(:result) { Role::Operation::Create.call(params) }

    it 'should execute successfully' do
      expect(result).to be_success
    end

    it 'create a new role instance' do
      expect(result['model']).to be_a(Role)
    end

    it 'should persist in the database' do
      expect do
        Role::Operation::Create.call(params)
      end.to change(Role, :count).by(1)
    end
  end
end

RSpec.describe Role::Operation::Create do
  context 'when invalid parameters are passed' do
    let(:params) { FactoryBot.attributes_for(:role, name: '') }
    let(:result) { Role::Operation::Create.call(params) }

    it 'should fail' do
      expect(result).to be_failure
    end

    it 'should not persist in the database' do
      expect do
        Role::Operation::Create.call(params)
      end.not_to change(Role, :count)
    end
  end
end
