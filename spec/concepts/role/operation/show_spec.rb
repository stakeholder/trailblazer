require 'rails_helper'

RSpec.describe Role::Operation::Show do
  context 'when valid parameters are passed' do
    let(:first_role) { FactoryBot.create(:role) }
    let(:second_role) { FactoryBot.create(:role) }
    let(:result) { Role::Operation::Show.call(id: second_role.id) }

    it 'should execute successfully' do
      expect(result).to be_success
    end

    it 'return a role instance' do
      expect(result['model']).to be_a(Role)
      expect(result['model'].name).to eq(second_role.name)
    end
  end

  context 'when invalid parameters are passed' do
    let(:result) { Role::Operation::Show.call(id: nil) }

    it 'should fail' do
      expect(result).to be_failure
    end
  end
end
