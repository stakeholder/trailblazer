require 'rails_helper'

RSpec.describe Role::Operation::Update do
  context 'when valid parameters are passed' do
    let(:role) { FactoryBot.create(:role) }
    let(:params) do
      { id: role.id, name: 'Admin', description: 'An administrator' }
    end
    let(:result) { Role::Operation::Update.call(params) }

    it 'should execute successfully' do
      expect(result).to be_success
    end

    it 'updates role in the database' do
      result
      role.reload
      expect(role.name).to eq('Admin')
      expect(role.description).to eq('An administrator')
    end
  end
end

RSpec.describe Role::Operation::Update do
  context 'when invalid parameters are passed' do
    let(:role) { FactoryBot.create(:role) }
    let(:params) { { id: role.id, name: '', description: 'An administrator' } }
    let(:result) { Role::Operation::Update.call(params) }

    it 'should fail' do
      expect(result).to be_failure
    end

    it 'should not update role in the database' do
      result
      role.reload
      expect(role.name).to_not eq('')
      expect(role.description).to_not eq('An administrator')
    end
  end
end
