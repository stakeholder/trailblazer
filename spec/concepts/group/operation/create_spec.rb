require 'rails_helper'

RSpec.describe Group::Operation::Create do
  context 'when valid params are received' do
    let(:originator) { FactoryBot.create(:originator) }
    let(:params) do
      FactoryBot.attributes_for(:group, originator_id: originator.id)
    end

    it 'should execute successfully' do
      result = Group::Operation::Create.call(params)
      expect(result).to be_success
    end
  end

  context 'when invalid association data is received' do
    let(:params) {  FactoryBot.attributes_for(:group) }

    it 'should fail with appropriate error messages' do
      result = Group::Operation::Create.call(params)
      expect(
        result['result.contract.default'].errors.messages.keys
      ).to include(:originator_id)
    end
  end
end
