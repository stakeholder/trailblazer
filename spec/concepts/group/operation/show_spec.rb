require 'rails_helper'

RSpec.describe Group::Operation::Show do
  context 'when a valid id is received' do
    let(:first_group) { FactoryBot.create(:group) }
    let(:second_group) { FactoryBot.create(:group) }
    let(:result) { Group::Operation::Show.call(id: second_group.id) }

    it 'should execute successfully' do
      expect(result).to be_success
    end

    it 'return a group instance' do
      expect(result['model']).to be_a(Group)
      expect(result['model'].name).to eq(second_group.name)
    end
  end

  context 'when invalid data is received' do
    let(:result) { Group::Operation::Show.call(id: nil) }

    it 'should fail' do
      expect(result.success?).to be false
    end
  end
end
