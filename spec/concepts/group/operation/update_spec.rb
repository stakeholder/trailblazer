require 'rails_helper'

RSpec.describe Group::Operation::Update do
  context 'when valid group data is received' do
    let(:group) { FactoryBot.create(:group) }
    let(:params) { { id: group.id, name: 'Just another group' } }
    let(:result) { Group::Operation::Update.call(params) }

    it 'should execute successfully' do
      expect(result).to be_success
    end

    it 'updates group in the database' do
      result
      group.reload
      expect(group.name).to eq('Just another group')
    end
  end
end

RSpec.describe Group::Operation::Update do
  context 'when one or more required params are not provided' do
    let(:group) { FactoryBot.create(:group) }
    let(:params) { { id: group.id, name: '' } }
    let(:result) { Group::Operation::Update.call(params) }

    it 'should fail with appropriate error messages' do
      expect(result.success?).to be false
    end

    it 'should not update group in the database' do
      result
      group.reload
      expect(group.name).to_not eq('')
    end
  end
end
