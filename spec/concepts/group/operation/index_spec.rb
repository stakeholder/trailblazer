require 'rails_helper'

RSpec.describe Group::Operation::Index do
  before do
    Group.delete_all
  end

  context 'when database is empty' do
    let(:result) { Group::Operation::Index.call }

    it 'should execute successfully' do
      expect(result).to be_success
    end

    it 'should return an empty array' do
      expect(result['models']).to be_empty
    end
  end

  context 'when database is not empty' do
    let(:originator) { FactoryBot.create(:originator) }
    let(:result) { Group::Operation::Index.call }

    it 'should execute successfully' do
      expect(result).to be_success
    end

    it 'should return an array with size of 10 ' do
      10.times { FactoryBot.create(:group, originator_id: originator.id) }
      expect(result['models'].count).to eq(10)
    end
  end
end
