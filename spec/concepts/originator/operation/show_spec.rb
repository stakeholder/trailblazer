require 'rails_helper'

RSpec.describe Originator::Operation::Show do
  context 'when a valid id is received' do
    let(:first_originator) { FactoryBot.create(:originator) }
    let(:second_originator) { FactoryBot.create(:originator) }
    let(:result) { Originator::Operation::Show.call(id: second_originator.id) }

    it 'should execute successfully' do
      expect(result).to be_success
    end

    it 'return an originator instance' do
      expect(result['model']).to be_a(Originator)
      expect(result['model'].legal_name).to eq(second_originator.legal_name)
    end
  end

  context 'when invalid data is received' do
    let(:result) { Originator::Operation::Show.call(id: nil) }

    it 'should fail' do
      expect(result.success?).to be false
    end
  end
end
