require 'rails_helper'

RSpec.describe Originator::Operation::Update do
  context 'when valid parameters are passed' do
    let(:originator) { FactoryBot.create(:originator) }
    let(:params) do
      { id: originator.id, legal_name: 'Pine Valley', display_name: 'PV' }
    end
    let(:result) { Originator::Operation::Update.call(params) }

    it 'should execute successfully' do
      expect(result).to be_success
    end

    it 'updates originator in the database' do
      result
      originator.reload
      expect(originator.legal_name).to eq('Pine Valley')
      expect(originator.display_name).to eq('PV')
    end
  end
end

RSpec.describe Originator::Operation::Update do
  context 'when invalid parameters are passed' do
    let(:originator) { FactoryBot.create(:originator) }
    let(:params) { { id: originator.id, legal_name: '' } }
    let(:result) { Originator::Operation::Update.call(params) }

    it 'should fail' do
      expect(result).to be_failure
    end

    it 'should not update originator in the database' do
      result
      originator.reload
      expect(originator.legal_name).to_not eq('')
    end
  end
end
