require 'rails_helper'

RSpec.describe Group::Operation::Index do
  before do
    Group.delete_all
    Originator.delete_all
  end

  context 'when database is empty' do
    let(:result) { Originator::Operation::Index.call }

    it 'should execute successfully' do
      expect(result).to be_success
    end

    it 'should return an empty array' do
      expect(result['models']).to be_empty
    end
  end

  context 'when database is not empty' do
    let(:originator_list) { FactoryBot.create_list(:originator, 10) }
    let(:result) { Originator::Operation::Index.call }

    it 'should execute successfully' do
      expect(result).to be_success
    end

    it 'should return an array with size of 10 ' do
      originator_list
      expect(result['models'].count).to eq(10)
    end
  end
end
