require 'rails_helper'

RSpec.describe Originator::Operation::Create do
  context 'when valid parameters are passed' do
    let(:params) do
      FactoryBot.attributes_for(:originator).merge(
        state_id: FactoryBot.create(:state).id
      )
    end
    let(:result) { Originator::Operation::Create.call(params) }

    it 'should execute successfully' do
      expect(result).to be_success
    end

    it 'create a new originator instance' do
      expect(result['model']).to be_a(Originator)
    end

    it 'should persist in the database' do
      expect do
        Originator::Operation::Create.call(params)
      end.to change(Originator, :count).by(1)
    end
  end
end

RSpec.describe Originator::Operation::Create do
  context 'when invalid parameters are passed' do
    let(:params) { FactoryBot.attributes_for(:originator, legal_name: '') }
    let(:result) { Originator::Operation::Create.call(params) }

    it 'should fail' do
      expect(result).to be_failure
    end

    it 'should not persist in the database' do
      expect do
        Originator::Operation::Create.call(params)
      end.not_to change(Originator, :count)
    end
  end
end
