require 'rails_helper'

RSpec.describe Permission::Operation::Show do
  context 'when valid parameters are passed' do
    let(:first_permission) { FactoryBot.create(:permission) }
    let(:second_permission) { FactoryBot.create(:permission) }
    let(:result) { Permission::Operation::Show.call(id: second_permission.id) }

    it 'should execute successfully' do
      expect(result).to be_success
    end

    it 'return a permission instance' do
      expect(result['model']).to be_a(Permission)
      expect(result['model'].name).to eq(second_permission.name)
    end
  end

  context 'when invalid parameters are passed' do
    let(:result) { Permission::Operation::Show.call(id: 100) }

    it 'should fail' do
      expect(result).to be_failure
    end
  end
end
