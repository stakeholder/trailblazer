require 'rails_helper'

RSpec.describe Permission::Operation::Update do
  context 'when valid parameters are passed' do
    let(:permission) { FactoryBot.create(:permission) }
    let(:params) do
      { id: permission.id, name: 'Create', description: 'User creation access' }
    end
    let(:result) { Permission::Operation::Update.call(params) }

    it 'should execute successfully' do
      expect(result).to be_success
    end

    it 'updates permission in the database' do
      result
      permission.reload
      expect(permission.name).to eq('Create')
      expect(permission.description).to eq('User creation access')
    end
  end
end

RSpec.describe Permission::Operation::Update do
  context 'when invalid parameters are passed' do
    let(:permission) { FactoryBot.create(:permission) }
    let(:params) do
      { id: permission.id, name: '', description: 'User creation access' }
    end
    let(:result) { Permission::Operation::Update.call(params) }

    it 'should fail' do
      expect(result).to be_failure
    end

    it 'should not update permission in the database' do
      result
      permission.reload
      expect(permission.name).to_not eq('')
      expect(permission.description).to_not eq('User creation access')
    end
  end
end
