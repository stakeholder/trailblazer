require 'rails_helper'

RSpec.describe Permission::Operation::Create do
  context 'when valid parameters are passed' do
    let(:params) { FactoryBot.attributes_for(:permission) }
    let(:result) { Permission::Operation::Create.call(params) }

    it 'should execute successfully' do
      expect(result).to be_success
    end

    it 'create a new permission instance' do
      expect(result['model']).to be_a(Permission)
    end

    it 'should persist in the database' do
      expect do
        Permission::Operation::Create.call(params)
      end.to change(Permission, :count).by(1)
    end
  end
end

RSpec.describe Permission::Operation::Create do
  context 'when invalid parameters are passed' do
    let(:params) { FactoryBot.attributes_for(:role, name: '') }
    let(:result) { Permission::Operation::Create.call(params) }

    it 'should fail' do
      expect(result).to be_failure
    end

    it 'should not persist in the database' do
      expect do
        Permission::Operation::Create.call(params)
      end.not_to change(Permission, :count)
    end
  end
end
