require 'rails_helper'

RSpec.describe Permission::Operation::Index do
  before do
    Permission.delete_all
  end

  context 'when database is empty' do
    let(:result) { Permission::Operation::Index.call }

    it 'should execute successfully' do
      expect(result).to be_success
    end

    it 'should return an empty array' do
      expect(result['models']).to be_empty
    end
  end

  context 'when database is not empty' do
    let(:result) { Permission::Operation::Index.call }

    it 'should execute successfully' do
      expect(result).to be_success
    end

    it 'should return an array ' do
      FactoryBot.create_list(:permission, 10)
      expect(result['models'].count).to eq(10)
    end
  end
end
