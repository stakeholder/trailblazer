require 'rails_helper'

RSpec.describe UserDetail::Operation::Create do
  context 'when valid parameters are passed' do
    let(:user) { FactoryBot.create(:user) }
    let(:params) do
      params = FactoryBot.attributes_for(:user_detail)
      params[:id] = user.id
      params
    end
    let(:result) { UserDetail::Operation::Create.call(params) }

    it 'should execute successfully' do
      expect(result).to be_success
    end

    it 'create a new user_detail' do
      expect(result['model']).to be_a(UserDetail)
    end

    it 'should create record in database' do
      expect do
        UserDetail::Operation::Create.call(params)
      end.to change(UserDetail, :count).by(1)
    end
  end
end

RSpec.describe UserDetail::Operation::Create do
  context 'when invalid parameters are passed' do
    let(:user) { FactoryBot.create(:user) }
    let(:params) do
      params = FactoryBot.attributes_for(:user_detail, first_name: '')
      params[:id] = user.id
      params
    end
    let(:result) { UserDetail::Operation::Create.call(params) }

    it 'should fail' do
      expect(result).to be_failure
    end

    it 'should not create record in database' do
      expect do
        UserDetail::Operation::Create.call(params)
      end.not_to change(UserDetail, :count)
    end
  end
end
