require 'rails_helper'

RSpec.describe UserDetail::Operation::Show do
  context 'when valid parameters are passed' do
    let(:user_detail) { FactoryBot.create(:user_detail) }
    let(:result) { UserDetail::Operation::Show.call(id: user_detail.user_id) }

    it 'should fetch successfully' do
      expect(result).to be_success
    end

    it 'return a user_detail instance' do
      expect(result['model']).to be_a(UserDetail)
      expect(result['model'].first_name).to eq(user_detail.first_name)
    end
  end

  # context "when invalid parameters are passed" do
  #   let(:result) { UserDetail::Operation::Show.({ id: nil }) }
  #
  #   it "should fail to return " do
  #     expect(result).to be_failure
  #   end
  # end
end
