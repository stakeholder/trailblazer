require 'rails_helper'

RSpec.describe UserDetail::Operation::Update do
  context 'when valid parameters are passed' do
    let(:user_detail) { FactoryBot.create(:user_detail) }
    let(:params) { { id: user_detail.id, last_name: 'Tested name' } }
    let(:result) { UserDetail::Operation::Update.call(params) }

    it 'should update successfully' do
      expect(result).to be_success
    end

    it 'updates user_detail' do
      result
      user_detail.reload
      expect(user_detail.last_name).to eq('Tested name')
    end
  end
end

RSpec.describe UserDetail::Operation::Update do
  context 'when invalid parameters are passed' do
    let(:user_detail) { FactoryBot.create(:user_detail) }
    let(:params) { { id: user_detail.id, first_name: '' } }
    let(:result) { UserDetail::Operation::Update.call(params) }

    it 'should fail to update' do
      expect(result).to be_failure
    end

    it 'should not update user_detail' do
      result
      user_detail.reload
      expect(user_detail.first_name).to_not eq('')
    end
  end
end
