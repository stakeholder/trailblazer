FactoryBot.define do
  factory :group do
    name { Faker::Name.unique.name }
    originator
  end
end
