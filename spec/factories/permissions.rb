FactoryBot.define do
  factory :permission do
    name { Faker::Lorem.word }
    description { Faker::Lorem.sentence }
    url_regex { Faker::Internet.url }
    verb { Faker::Types.string }
  end
end
