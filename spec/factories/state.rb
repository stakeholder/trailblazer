FactoryBot.define do
  factory :state do
    name { Faker::Name.unique.name }
    abbreviation { Faker::Name.unique.name }
    state_fips { Faker::Number.number }
    created_at { Faker::Lorem.characters(5) }
    updated_at { Faker::Lorem.characters(5) }
  end
end
