FactoryBot.define do
  factory :originator do
    legal_name { Faker::Name.unique.name }
    display_name { Faker::Name.unique.name }
    address { Faker::Address.street_name }
    address2 { Faker::Address.street_address }
    city { Faker::Address.city }
    state
    zip_code { Faker::Address.zip_code }
  end
end
