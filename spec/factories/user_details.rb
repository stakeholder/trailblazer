FactoryBot.define do
  factory :user_detail do
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    mobile_phone { Faker::PhoneNumber.phone_number }
    office_phone { Faker::PhoneNumber.phone_number }
    user
  end
end
