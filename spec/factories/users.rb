FactoryBot.define do
  factory :user do
    email { Faker::Internet.email }
    role
    originator
  end
end
