require 'api_constraints'

Rails.application.routes.draw do
  resources :users do
    get 'details', on: :member, to: 'users#show_details', prefix: 'user_details'
    post 'details', on: :member, to: 'users#update_details', prefix: 'user_details'
    post 'change_password', on: :collection
  end

  resources :vendors, :originators, :certificates, :groups, :permissions, :roles, :vendor_types
  resources :states, only: [:index]

  resources :docs, controller: 'apidocs', only: :index
end
