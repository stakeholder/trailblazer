# Load the Rails application.
require_relative 'application'

# Initialize the Rails application.
Rails.application.initialize!


# ActionMailer Configuration Settings
ActionMailer::Base.smtp_settings = {
  :user_name => ENV["USERNAME"],
  :password => ENV["PASSWORD"],
  :domain => ENV["DOMAIN"],
  :address => ENV["ADDRESS"],
  :port => ENV["PORT"],
  :authentication => :plain,
  :enable_starttls_auto => true
}
