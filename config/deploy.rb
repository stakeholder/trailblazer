lock '3.10.1'

set :stages, ['staging']
set :default_stage, 'staging'
set :ssh_options, { :forward_agent => true }


set :application, 'terminus_user_service'
set :repo_url, 'git@gitlab.com:project-terminus/users-service.git'

set :rbenv_type, :user # or :system, depends on your rbenv setup
set :rbenv_ruby, '2.5.0'

# in case you want to set ruby version from the file:

set :rbenv_prefix, "RBENV_ROOT=#{fetch(:rbenv_path)} RBENV_VERSION=#{fetch(:rbenv_ruby)} #{fetch(:rbenv_path)}/bin/rbenv exec"
set :rbenv_map_bins, %w{rake gem bundle ruby rails}
set :rbenv_roles, :all
