set :deploy_to, '/var/www/users-service/html'
set :branch, 'develop'
set :rails_env, 'staging'
server 'api.projectterminus.io', user: 'terminus', roles: %w{web app db}, primary: true
set :port, '4000'

namespace :deploy do
  task :install_bundle do
    on roles :web do
      within "#{current_path}" do
        with rails_env: "#{fetch(:default_stage)}" do
          execute :bundle, "install"
        end
      end
    end
  end

  task :migrate_db do
    on roles :web do
      within "#{current_path}" do
        with rails_env: "#{fetch(:default_stage)}" do
          execute :rails, "db:migrate"
        end
      end
    end
  end

  task :seed_db do
    on roles :web do
      within "#{current_path}" do
        with rails_env: "#{fetch(:default_stage)}" do
          execute :rails, "db:seed"
        end
      end
    end
  end

  task :restart do
    on roles :web do
      within "#{current_path}" do
        with rails_env: "#{fetch(:default_stage)}" do
          execute "cd /var/www/users-service/html/current && if [ $(lsof -t -i:4000 | wc -c) -gt 2 ]; then kill -9 $(lsof -t -i:4000) && passenger start -p 4000 -d -e development; else passenger start -p 4000 -d -e development; fi"
        end
      end
    end
  end

  task :copy_shared_files do
    on roles :web do
      within "#{current_path}" do
        with rails_env: "#{fetch(:default_stage)}" do
          execute "cp /var/www/users-service/html/shared/application.yml  /var/www/users-service/html/current/config/application.yml"
          execute "cp /var/www/users-service/html/shared/database.yml  /var/www/users-service/html/current/config/database.yml"
          execute "cp /var/www/users-service/html/shared/secrets.yml  /var/www/users-service/html/current/config/secrets.yml"
        end
      end
    end
  end
end


after 'deploy:finishing', 'deploy:install_bundle'
after 'deploy:install_bundle', 'deploy:copy_shared_files'
after 'deploy:copy_shared_files', 'deploy:migrate_db'
after 'deploy:migrate_db', 'deploy:seed_db'
after 'deploy:seed_db', 'deploy:restart'
