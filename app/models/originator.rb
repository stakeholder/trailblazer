# This class models the Originator resource and its associations with group
# and user resource
class Originator < ApplicationRecord
  extend Swagger::Originator::Model
  Originator.load_swagger

  scope :terminus, -> { where(legal_name: 'terminus') }
  scope :default, -> { where("legal_name != 'terminus'") }

  belongs_to :state
  has_many :groups
  has_many :users
end
