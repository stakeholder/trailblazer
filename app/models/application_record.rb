# This is the base model class
class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
  include Swagger::Blocks
end
