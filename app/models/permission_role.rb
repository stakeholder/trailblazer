# This class models the relationship between permission and role resource
class PermissionRole < ApplicationRecord
  belongs_to :role
  belongs_to :permission
end
