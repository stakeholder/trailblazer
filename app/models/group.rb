# This class models the Group resource and its associations with originator
# and user resource
class Group < ApplicationRecord
  extend Swagger::Group::Model

  Group.load_swagger
  belongs_to :originator
  has_many :group_users,
           class_name: 'GroupUser'
  has_many :users,
           class_name: 'User',
           through: :group_users
end
