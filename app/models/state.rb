# This class models the State resource and its associations with originator
# and vendor resource
class State < ApplicationRecord
  has_many :originators, class_name: 'Originator'
  has_many :vendors, class_name: 'Vendor'
end
