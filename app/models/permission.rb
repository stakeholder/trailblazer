# This class models the Permission resource and its association with Role
# resource
class Permission < ApplicationRecord
  extend Swagger::Permission::Model
  after_create :grant_owner_access
  validates :url_regex, presence: true, uniqueness: { scope: :verb }

  has_many :permission_roles,
           class_name: 'PermissionRole'
  has_many :roles,
           through: :permission_roles

  Permission.load_swagger_schema
  Permission.load_swagger_input_schema

  private

  def grant_owner_access
    permission_roles.create(role: Role.unscoped.first)
  end
end
