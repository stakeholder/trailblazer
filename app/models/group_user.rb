# This class models the relationship between User and Group resource
class GroupUser < ApplicationRecord
  belongs_to :user
  belongs_to :group
end
