require 'bcrypt'
# This class models the User resource and its associations with originator
# vendor, role, userDetail and group resource
class User < ApplicationRecord
  extend Swagger::User::Model
  User.load_swagger

  scope :for_originator, (lambda do |originator_id|
    where('originator_id = ?', originator_id)
  end)
  before_save :hash_user_password

  belongs_to :originator
  belongs_to :role
  has_one :user_detail
  has_many :group_users,
           class_name: 'GroupUser'
  has_many :groups,
           class_name: 'Group',
           through: :group_users

  private

  def hash_user_password
    self.password = BCrypt::Password.create password
  end
end
