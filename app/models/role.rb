# This class models the Role resource and its associations with permission
# and user resource
class Role < ApplicationRecord
  extend Swagger::Role::Model
  scope :terminus, -> { where("name = 'Owner'") }
  scope :default, -> { where("name = 'Regular'") }

  has_many :users
  has_many :permission_roles,
           class_name: 'PermissionRole'
  has_many :permissions,
           through: :permission_roles

  Role.load_swagger_schema
  Role.load_swagger_input_schema
end
