# This class models the UserDetail resource and its associations with user
# resource
class UserDetail < ApplicationRecord
  extend Swagger::UserDetail::Model
  UserDetail.load_swagger

  belongs_to :user
end
