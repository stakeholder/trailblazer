# This controller handles requests to the application documentation
class ApidocsController < ApplicationController
  include Swagger::Blocks

  swagger_root do
    key :swagger, '2.0'
    info do
      key :version, '1.0.0'
      key :title, 'Terminus'
      key :description, 'Terminus API documentation'
      contact do
        key :name, 'evan@pinevalleyre.com'
      end
    end
    tag do
      key :name, 'Terminus'
      key :description, 'Real Estate'
    end
    key :host, 'localhost:3000'
    key :basePath, '/'
    key :consumes, ['application/json']
    key :produces, ['application/json']
  end

  # Add list of all classes that have swagger_* declarations.
  SWAGGERED_CLASSES = [
    User,
    UserDetail,
    Group,
    VendorType,
    Vendor,
    Originator,
    UsersController,
    GroupsController,
    VendorsController,
    VendorTypesController,
    OriginatorsController,
    RolesController,
    Role,
    PermissionsController,
    Permission,
    self
  ].freeze

  def index
    render json: Swagger::Blocks.build_root_json(SWAGGERED_CLASSES)
  end
end
