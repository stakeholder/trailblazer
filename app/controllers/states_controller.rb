# This controller handles State resource related request
class StatesController < ApplicationController
  def index
    run State::Operation::Index do |result|
      return json_response(result, true)
    end
    json_error_response result
  end
end
