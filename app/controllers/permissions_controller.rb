# This controller handles Permission resource related request
class PermissionsController < ApplicationController
  extend Swagger::Permission::Controller

  PermissionsController.load_swagger

  def index
    run Permission::Operation::Index do |result|
      return json_response(result, true)
    end
    json_error_response result
  end

  def create
    run Permission::Operation::Create do |result|
      return json_response result
    end
    json_error_response result
  end

  def update
    run Permission::Operation::Update do |result|
      return json_response result
    end
    json_error_response result
  end

  def show
    run Permission::Operation::Show do |result|
      return json_response result
    end
    json_error_response result
  end
end
