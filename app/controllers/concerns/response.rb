# Formats json response for both single resource and collection
module Response
  def json_response(result, collection = false, status = :ok)
    unless collection
      return render json: format_response(render_single(result)),
                    status: status
    end
    render  json: format_response(render_collection(result)),
            status: status
  end

  def render_collection(result)
    result['representer.render.class'].for_collection.new(result['models'])
                                      .to_hash
  end

  def render_single(result)
    result['representer.render.class'].new(result['model'])
                                      .to_hash
  end

  def json_error_response(result)
    render  json: result['error'],
            status: result['error'][:status]
  end

  def format_response(response, success = true)
    { success: success, data: response }
  end
end
