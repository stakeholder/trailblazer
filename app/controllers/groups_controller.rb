# This controller handles Group resource related request
class GroupsController < ApplicationController
  extend Swagger::Group::Controller
  GroupsController.load_swagger

  def create
    run Group::Operation::Create do |result|
      return json_response result
    end
    json_error_response result
  end

  def index
    run Group::Operation::Index do |result|
      return json_response(result, true)
    end
    json_error_response result
  end

  def show
    run Group::Operation::Show do |result|
      return json_response result
    end
    json_error_response result
  end

  def update
    run Group::Operation::Update do |result|
      return json_response result
    end
    json_error_response result
  end

  def destroy
    run Group::Operation::Delete do |result|
      return json_response result
    end
    json_error_response result
  end
end
