# This controller handles Originator resource related request
class OriginatorsController < ApplicationController
  extend Swagger::Originator::Controller
  OriginatorsController.load_swagger

  def create
    run Originator::Operation::Create do |result|
      return json_response result
    end
    json_error_response result
  end

  def index
    run Originator::Operation::Index do |result|
      return json_response(result, true)
    end
    json_error_response result
  end

  def show
    run Originator::Operation::Show do |result|
      return json_response result
    end
    json_error_response result
  end

  def update
    run Originator::Operation::Update do |result|
      return json_response result
    end
    json_error_response result
  end

  def destroy
    run Originator::Operation::Delete do |result|
      return json_response result
    end
    json_error_response result
  end
end
