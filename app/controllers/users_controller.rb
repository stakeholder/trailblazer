# This controller handles User resource related request
class UsersController < ApplicationController
  extend Swagger::User::Controller
  UsersController.load_swagger

  def create
    run User::Operation::Create do |result|
      return json_response result
    end
    json_error_response result
  end

  def index
    run User::Operation::Index do |result|
      return json_response(result, true)
    end
    json_error_response result
  end

  def show
    run User::Operation::Show do |result|
      return json_response result
    end
    json_error_response result
  end

  def update
    run User::Operation::Update do |result|
      return json_response result
    end
    json_error_response result
  end

  def destroy
    run User::Operation::Delete do |result|
      return json_response result
    end
    json_error_response result
  end

  def show_details
    run UserDetail::Operation::Show do |result|
      return json_response result
    end
    json_error_response result
  end

  def update_details
    run UserDetail::Operation::Update do |result|
      return json_response result
    end
    json_error_response result
  end

  def change_password
    run User::Operation::ChangePassword do |result|
      return json_response result
    end
    json_error_response result
  end
end
