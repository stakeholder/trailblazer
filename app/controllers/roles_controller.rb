# This controller handles Role resource related request
class RolesController < ApplicationController
  extend Swagger::Role::Controller

  RolesController.load_swagger

  def index
    run Role::Operation::Index do |result|
      return json_response(result, true)
    end
    json_error_response result
  end

  def create
    params.permit!
    run Role::Operation::Create do |result|
      return json_response result
    end
    json_error_response result
  end

  def update
    run Role::Operation::Update do |result|
      return json_response result
    end
    json_error_response result
  end

  def show
    run Role::Operation::Show do |result|
      return json_response result
    end
    json_error_response result
  end
end
