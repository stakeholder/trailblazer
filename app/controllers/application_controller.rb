require 'base64'
require 'json'
# The base controller class
class ApplicationController < ActionController::API
  include Response
  include Swagger::Blocks
  before_action :extract_payload_from_token

  def extract_payload_from_token
    token = request.headers['Authorization'].split.second
    payload = JSON.parse(Base64.decode64(token.split('.').second))
    formatted_payload = {}
    payload.each do |key, value|
      formatted_payload[key.to_s.underscore.to_sym] = value
    end
    params[:payload] = formatted_payload
  end
end
