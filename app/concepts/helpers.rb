# This module holds helper methods used across all concept operations
module Helpers
  def verify_id!(_options, params:, **)
    id = params['id'] || params[:id]
    !id.to_i.zero?
  end

  def compose_error_response(status = 400, error = 'Server Error',
                             message = 'An unexpected error occurred')
    { success: false, status: status, error: error, message: message }
  end

  def does_model_exist?(options, *)
    options['error'] = compose_error_response(
      404,
      'Not Found',
      "#{options['model.class']} does not exist"
    )
  end

  def type_error!(options, params:, **)
    options['error'] = compose_error_response(
      400,
      'Type Error',
      "#{params[:id]} must be an integer"
    )
  end

  def log_error!(options)
    error = options['contract.default'].errors.messages
    options['error'] = {
      success: false,
      status: error.empty? ? 500 : 400,
      error: error.empty? ? 'Server Error' : 'ValueException',
      message: error.empty? ? 'An unexpected error occurred' : error
    }
  end

  def belongs_to_terminus_admin?(originator_id)
    # we check if the user making the request belongs to terminus admin
    Originator.terminus.first.id == originator_id.to_i
  end

  def extract_payload(params)
    # we extract and return the payload hash or we return nil
    params.key?(:payload) ? params[:payload] : nil
  end

  def verify_originator_scope!(_options, params:, **)
    payload = extract_payload params
    if payload
      originator_id = User.find(params[:id]).originator_id
      return originator_id == payload[:originator_id].to_i
    end
    false
  end
end
