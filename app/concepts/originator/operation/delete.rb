module Originator::Operation
  # This is the operation for originator resource delete action
  class Delete < Trailblazer::Operation
    extend Representer::DSL

    representer :render, Originator::Representer

    step Model(Originator, :find_by), name: 'terminus.model.find'
    step :mark_as_deleted!, name: 'terminus.action.delete'

    def mark_as_deleted!(options, model:)
      # TODO: model.update({ is_deleted: true })
    end
  end
end
