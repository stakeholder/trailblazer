module Originator::Operation
  # This is the operation for originator resource show action
  class Show < Trailblazer::Operation
    extend Representer::DSL

    step Nested(Originator::Operation::Edit)

    representer :render, Originator::Representer
  end
end
