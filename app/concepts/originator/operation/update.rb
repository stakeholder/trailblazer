module Originator::Operation
  # This is the operation for originator resource update action
  class Update < Trailblazer::Operation
    include Helpers
    extend Representer::DSL

    step     Nested(Originator::Operation::Edit), fail_fast: true
    step     Contract::Validate(), name: 'terminus.contract.validate'
    step     Contract::Persist(), name: 'terminus.contract.persist'
    failure  :log_error!, name: 'terminus.failure.log'

    representer :render, Originator::Representer
  end
end
