module Originator::Operation
  # This is the operation for originator resource edit action
  class Edit < Trailblazer::Operation
    include Helpers

    step     :verify_id!
    failure  :type_error!, fail_fast: true
    step     Model(Originator, :find_by)
    failure  :does_model_exist?, fail_fast: true
    step     Contract::Build(constant: Originator::Contract)
  end
end
