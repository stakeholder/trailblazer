module Originator::Operation
  # This is the operation for originator resource create action
  class Create < Trailblazer::Operation
    include Helpers
    extend Representer::DSL

    representer :render, Originator::Representer

    step     Model(Originator, :new), name: 'terminus.model.build'
    step     Contract::Build(constant: Originator::Contract),
             name: 'terminus.contract.build'
    step     Contract::Validate(), name: 'terminus.contract.validate'
    step     Contract::Persist(), name: 'terminus.contract.persist'
    failure  :log_error!, name: 'terminus.failure.log'
  end
end
