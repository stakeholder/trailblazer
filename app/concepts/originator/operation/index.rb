module Originator::Operation
  # This is the operation for originator resource index action
  class Index < Trailblazer::Operation
    include Helpers
    extend Representer::DSL

    representer :render, Originator::Representer

    step     :all!, name: 'terminus.model.list'
    failure  :log_error!

    def all!(options, params:, **)
      payload = extract_payload params
      if payload && !belongs_to_terminus_admin?(payload[:originator_id])
        options['models'] = Originator.default
      else
        options['models'] = Originator.all.order(:legal_name)
      end
    end
  end
end
