require 'reform/form/validation/unique_validator'
# This is the contract class for Originator resource
class Originator::Contract < Reform::Form
  property :legal_name
  property :display_name
  property :address
  property :address2
  property :city
  property :state_id
  property :zip_code

  validates :legal_name, presence: true, unique: true
  validates :display_name, presence: true
  validates :address, presence: true
  validates :city, presence: true
  validates :state_id, presence: true
  validates :zip_code, presence: true
end
