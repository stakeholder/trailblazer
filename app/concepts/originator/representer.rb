# This is the json response representer for Originator resource
class Originator::Representer < Roar::Decorator
  include Roar::JSON

  property :id
  property :legal_name
  property :display_name
  property :address
  property :address2
  property :city
  property :state_id
  property :zip_code

  property :state do
    property :id
    property :name
    property :abbreviation
    property :state_fips
  end
end
