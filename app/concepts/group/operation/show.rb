module Group::Operation
  # This is the operation for group resource show action
  class Show < Trailblazer::Operation
    extend Representer::DSL

    step Nested(Group::Operation::Edit)

    representer :render, Group::Representer
  end
end
