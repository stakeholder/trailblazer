module Group::Operation
  # This is the operation for group resource index action
  class Index < Trailblazer::Operation
    extend Representer::DSL

    representer :render, Group::Representer

    step    :model!, name: 'terminus.model.list'
    failure :log_error!

    def model!(options, *)
      options['models'] = Group.all.order(:name)
    end
  end
end
