module Group::Operation
  # This is the operation for group resource edit action
  class Edit < Trailblazer::Operation
    include Helpers

    step     :verify_id!
    failure  :type_error!, fail_fast: true
    step     Model(Group, :find_by)
    failure  :does_model_exist?, fail_fast: true
    step     Contract::Build(constant: Group::Contract)
  end
end
