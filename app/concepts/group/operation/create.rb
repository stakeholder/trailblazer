module Group::Operation
  # This is the operation for group resource create action
  class Create < Trailblazer::Operation
    include Helpers
    extend Representer::DSL

    representer :render, Representer

    step     Model(Group, :new), name: 'terminus.model.build'
    step     Contract::Build(constant: Group::Contract),
             name: 'terminus.contract.build'
    step     Contract::Validate(), name: 'terminus.contract.validate'
    step     Contract::Persist(), name: 'terminus.contract.persist'
    failure  :log_error!, name: 'terminus.failure.log'
  end
end
