module Group::Operation
  # This is the operation for group resource update action
  class Update < Trailblazer::Operation
    include Helpers
    extend Representer::DSL

    step     Nested(Group::Operation::Edit), fail_fast: true
    step     Contract::Validate(), name: 'terminus.contract.validate'
    step     Contract::Persist(), name: 'terminus.contract.persist'
    failure  :log_error!, name: 'terminus.failure.log'

    representer :render, Group::Representer
  end
end
