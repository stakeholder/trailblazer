module Group::Operation
  # This is the operation for group resource delete action
  class Delete < Trailblazer::Operation
    extend Representer::DSL

    representer :render, Group::Representer

    step Model(Group, :find_by), name: 'terminus.model.find'
    # step delete!, name: "terminus.action.delete"

    # def delete!(options, model:)
    #   # TODO check for existing users before deleting group
    # end
  end
end
