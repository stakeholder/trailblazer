# This is the json response representer for Group resource
class Group::Representer < Roar::Decorator
  include Roar::JSON

  property :id
  property :name

  property :originator do
    property :legal_name
    property :display_name
  end
end
