require 'reform/form/validation/unique_validator'
# This is the contract class for Group resource
class Group::Contract < Reform::Form
  property :name
  property :originator_id

  validates :name, presence: true, unique: true
  validates :originator_id, presence: true, format: { with: /\d/ }
end
