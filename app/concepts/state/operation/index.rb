module  State::Operation
  # This is the operation for vendor resource index action
  class Index < Trailblazer::Operation
    include Helpers
    extend Representer::DSL

    representer :render, State::Representer

    step    :all!
    failure :log_error!

    def all!(options, *)
      options['models'] = State.all.order(:name)
    end
  end
end
