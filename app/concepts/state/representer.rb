# This is the json reponse representer for the State resource
class State::Representer < Roar::Decorator
  include Roar::JSON

  property :id
  property :name
end
