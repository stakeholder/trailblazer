# This is the json response representer for User resource
class User::Representer < Roar::Decorator
  include Roar::JSON

  property :id
  property :email

  property :role do
    property :id
    property :name
  end

  property :originator do
    property :id
    property :legal_name
    property :display_name
  end

  property :user_detail do
    property :first_name
    property :last_name
    property :mobile_phone
    property :office_phone
  end
end
