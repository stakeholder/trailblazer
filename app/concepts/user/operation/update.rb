module  User::Operation
  # This is the operation for user resource update action
  class Update < Trailblazer::Operation
    include Helpers
    extend Representer::DSL

    step     Nested(User::Operation::Edit), fail_fast: true
    step     Contract::Validate(), name: 'terminus.contract.validate'
    step     Contract::Persist(), name: 'terminus.contract.persist'
    failure  :log_error!, name: 'terminus.failure.log'

    representer :render, User::Representer
  end
end
