require 'securerandom'
module  User::Operation
  # This is the operation for user resource create action
  class Create < Trailblazer::Operation
    include Helpers
    extend Representer::DSL

    step     Model(User, :new), name: 'terminus.model.build'
    step     :generate_password!
    step     :assign_originator!
    step     :assign_default_role!
    step     Contract::Build(constant: User::Contract),
             name: 'terminus.contract.build'
    step     Contract::Validate(), name: 'terminus.contract.validate'
    step     Contract::Persist(), name: 'terminus.contract.persist'
    step     :insert_user_details!
    failure  :log_user_creation_error!, fail_fast: true
    step     :send_signup_email!
    failure  :log_error!, name: 'terminus.failure.log'

    representer :render, User::Representer

    private

    def assign_originator!(_options, params:, **)
      payload = extract_payload params
      if payload && !belongs_to_terminus_admin?(payload[:originator_id])

        params[:originator_id] = payload[:originator_id].to_i
      end
      params[:originator_id]
    end

    def assign_default_role!(_options, params:, **)
      params[:role_id] = Role.default.first.id unless params.key?(:role_id)
      params[:role_id]
    end

    def generate_password!(_options, params:, **)
      params[:password] = SecureRandom.hex(10)
    end

    def insert_user_details!(_options, params:, model:, **)
      user_detail = params[:user_detail]
      user_detail[:user_id] = model.id
      user_detail_instance = UserDetail.new(
        last_name: user_detail[:last_name],
        first_name: user_detail[:first_name],
        user_id: user_detail[:user_id],
        mobile_phone: user_detail[:mobile_phone],
        office_phone: user_detail[:office_phone]
      ).save
      user_detail_instance ? true : false
    end

    def log_user_creation_error!(options, model:, **)
      model.id && User.find(model.id).delete
      options['error'] = compose_error_response(
        400,
        'bad request',
        'Unable to create user'
      )
    end

    def send_signup_email!(_options, params:, **)
      return true if Rails.env.test?

      UserNotifierMailer.send_signup_email(params).deliver_now
    end
  end
end
