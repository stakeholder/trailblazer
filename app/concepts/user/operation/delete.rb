module  User::Operation
  # This is the operation for user resource delete action
  class Delete < Trailblazer::Operation
    extend Representer::DSL

    representer :render, User::Representer

    step Nested(User::Operation::Edit)
    step :mark_as_deleted!, name: 'terminus.action.delete'

    def mark_as_deleted!(options, model:)
      # TODO: model.update({ is_deleted: true })
    end
  end
end
