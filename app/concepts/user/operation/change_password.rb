require 'bcrypt'

module  User::Operation
  # This is the operation for user resource update action
  class ChangePassword < Trailblazer::Operation
    include Helpers
    extend Representer::DSL

    step     :verify_id!
    failure  :type_error!, fail_fast: true
    step     Model(User, :find_by)
    failure  :does_model_exist?, fail_fast: true
    step     :update_password!, fail_fast: true
    step     Contract::Build(constant: User::Contract)
    step     Contract::Validate(), name: 'terminus.contract.validate'
    step     Contract::Persist(), name: 'terminus.contract.persist'
    failure  :log_error!, name: 'terminus.failure.log'

    representer :render, User::Representer

    def update_password!(options, params:, **)
      if BCrypt::Password.new(options['model'][:password]) ==
         params[:current_password]
        options['model'][:password] = params[:new_password]
      else
        options['error'] = compose_error_response(
          400,
          'Passwords provided do not match',
          'InvalidParamsError'
        )
      end
    end
  end
end
