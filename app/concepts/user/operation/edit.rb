module User::Operation
  # This is the operation for user resource edit action
  class Edit < Trailblazer::Operation
    include Helpers

    step     :verify_id!
    failure  :type_error!, fail_fast: true
    step     Model(User, :find_by)
    failure  :does_model_exist?, fail_fast: true
    step     :verify_originator_scope!
    failure  :log_authorization_error!, fail_fast: true
    step     Contract::Build(constant: User::Contract)

    private

    def log_authorization_error!(options, params:, **)
      options['error'] = compose_error_response(
        401,
        'AuthorizationError',
        "Unauthorized to #{params[:action]} this user"
      )
    end
  end
end
