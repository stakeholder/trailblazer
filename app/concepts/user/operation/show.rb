module  User::Operation
  # This is the operation for user resource show action
  class Show < Trailblazer::Operation
    extend Representer::DSL

    step   Nested(User::Operation::Edit)

    representer :render, User::Representer
  end
end
