module  User::Operation
  # This is the operation for user resource index action
  class Index < Trailblazer::Operation
    include Helpers
    extend Representer::DSL

    representer :render, User::Representer

    step    :all!, name: 'terminus.model.list'
    failure :log_error!

    private

    def all!(options, params:, **)
      # we need to check and extract payload hash from params if any
      payload = extract_payload params
      # we need to call the for_originator scope in the user model
      # only if the user does not belong to the terminus_admin
      if payload && !belongs_to_terminus_admin?(payload[:originator_id])
        options['models'] = User.for_originator(payload[:originator_id].to_i)
                                .where(generate_query_params(params))
        return options
      end
      options['models'] = User.where(generate_query_params(params))
    end

    def generate_query_params(params)
      # we need to generate query parameters in order to filter the query result
      query_params = {}
      if params.key?('role')
        query_params['role'] = Role.find_by_name(params['role'])
      end
      query_params
    end
  end
end
