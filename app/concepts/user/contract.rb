require 'reform/form/validation/unique_validator'
# This is the contract class for User resource
class User::Contract < Reform::Form
  property :email
  property :password
  property :originator_id
  property :role_id

  validates :email, presence: true, unique: true
  validates :originator_id, presence: true, format: { with: /\d/ }
  validates :role_id, presence: true, format: { with: /\d/ }
end
