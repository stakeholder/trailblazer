# This is the json response representer for Permission resource
class Permission::Representer < Representable::Decorator
  include Roar::JSON

  property :id
  property :name
  property :description
  property :verb
  property :url_regex
  property :created_at
  property :updated_at
end
