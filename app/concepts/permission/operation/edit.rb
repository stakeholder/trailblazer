module Permission::Operation
  # This is the operation for permission resource edit action
  class Edit < Trailblazer::Operation
    include Helpers

    step     :verify_id!
    failure  :type_error!, fail_fast: true
    step     Model(Permission, :find_by)
    failure  :does_model_exist?, fail_fast: true
    step     Contract::Build(constant: Permission::Contract)
  end
end
