module Permission::Operation
  # This is the operation for permission resource create action
  class Create < Trailblazer::Operation
    include Helpers
    extend Representer::DSL

    step     Nested(Permission::Operation::New)
    step     Contract::Validate()
    step     Contract::Persist()
    failure  :log_error!

    representer :render, Permission::Representer
  end
end
