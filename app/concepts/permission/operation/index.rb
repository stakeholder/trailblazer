module Permission::Operation
  # This is the operation for permission resource index action
  class Index < Trailblazer::Operation
    include Helpers
    extend Representer::DSL

    representer :render, Permission::Representer

    step     :all!
    failure  :log_error!

    def all!(options)
      options['models'] = Permission.all.order(:name)
    end
  end
end
