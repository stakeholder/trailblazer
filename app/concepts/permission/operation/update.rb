module Permission::Operation
  # This is the operation for permission resource update action
  class Update < Trailblazer::Operation
    include Helpers
    extend Representer::DSL

    step     Nested(Permission::Operation::Edit), fail_fast: true
    step     Contract::Validate()
    step     Contract::Persist()
    failure  :log_error!

    representer :render, Permission::Representer
  end
end
