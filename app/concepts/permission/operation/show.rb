module Permission::Operation
  # This is the operation for permission resource show action
  class Show < Trailblazer::Operation
    extend Representer::DSL

    step Nested(Permission::Operation::Edit)

    representer :render, Permission::Representer
  end
end
