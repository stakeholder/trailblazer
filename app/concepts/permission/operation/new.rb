module Permission::Operation
  # This is the operation for permission resource new action
  class New < Trailblazer::Operation
    step     Model(Permission, :new)
    step     Contract::Build(constant: Permission::Contract)
  end
end
