require 'reform/form/validation/unique_validator'
# This is the contract class for Permission resource
class Permission::Contract < Reform::Form
  property :name
  property :description
  property :verb
  property :url_regex

  validates :name, presence: true, unique: true
  validates :description, presence: true
  validates :verb, presence: true
  validates :url_regex, presence: true, unique: { scope: :verb }
end
