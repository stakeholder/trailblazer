# This is the json response representer for Role resource
class Role::Representer < Representable::Decorator
  include Roar::JSON

  property :id
  property :name
  property :description
  property :created_at
  property :updated_at
end
