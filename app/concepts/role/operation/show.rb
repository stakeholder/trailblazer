module Role::Operation
  # This is the operation for role resource show action
  class Show < Trailblazer::Operation
    extend Representer::DSL

    step Nested(Role::Operation::Edit)

    representer :render, Role::Representer
  end
end
