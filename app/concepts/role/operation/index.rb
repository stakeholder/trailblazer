module Role::Operation
  # This is the operation for role resource index action
  class Index < Trailblazer::Operation
    include Helpers
    extend Representer::DSL

    representer :render, Role::Representer

    step     :all!
    failure  :log_error!

    def all!(options)
      options['models'] = Role.all.order(:name)
    end
  end
end
