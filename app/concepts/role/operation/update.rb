module Role::Operation
  # This is the operation for role resource update action
  class Update < Trailblazer::Operation
    include Helpers
    extend Representer::DSL

    step     Nested(Role::Operation::Edit), fail_fast: true
    step     Contract::Validate()
    step     Contract::Persist()
    failure  :log_error!

    representer :render, Role::Representer
  end
end
