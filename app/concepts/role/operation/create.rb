module Role::Operation
  # This is the operation for role resource create action
  class Create < Trailblazer::Operation
    include Helpers
    extend Representer::DSL
    representer :render, Role::Representer

    step     Nested(Role::Operation::New)
    step     Contract::Validate()
    step     Contract::Persist()
    step     :save_role_permissions!

    failure  :log_error!

    def save_role_permissions!(options)
      role = options['model']
      permissions_for_role = options['params'][:permissions]
      role.permissions = permissions_for_role if permissions_for_role
    end
  end
end
