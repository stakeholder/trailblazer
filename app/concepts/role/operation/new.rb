module Role::Operation
  # This is the operation for role resource new action
  class New < Trailblazer::Operation
    step     Model(Role, :new)
    step     Contract::Build(constant: Role::Contract)
  end
end
