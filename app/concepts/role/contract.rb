require 'reform/form/validation/unique_validator'
# This is the contract class for Role resource
class Role::Contract < Reform::Form
  property :name
  property :description
  collection :permission_roles
  validates :name, presence: true, unique: true
  validates :description, presence: true
end
