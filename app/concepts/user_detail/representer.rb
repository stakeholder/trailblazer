# This is the json response representer for user detail resource
class UserDetail::Representer < Roar::Decorator
  include Roar::JSON

  property :id
  property :first_name
  property :last_name
  property :mobile_phone
  property :office_phone
  property :user_id
end
