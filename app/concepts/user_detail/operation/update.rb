module  UserDetail::Operation
  # This is the operation for user detail resource update action
  class Update < Trailblazer::Operation
    include Helpers
    extend Representer::DSL

    step     Nested(UserDetail::Operation::Edit)
    step     Contract::Validate(), name: 'terminus.contract.validate'
    step     Contract::Persist(), name: 'terminus.contract.persist'
    failure  :log_error!, name: 'terminus.failure.log'

    representer :render, UserDetail::Representer
  end
end
