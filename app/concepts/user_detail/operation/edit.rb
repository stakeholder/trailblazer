module UserDetail::Operation
  # This is the operation for user detail resource edit action
  class Edit < Trailblazer::Operation
    include Helpers

    step     :verify_id!
    failure  :type_error!, fail_fast: true
    step     Model(UserDetail, :find_by)
    failure  :does_model_exist?, fail_fast: true
    step     Contract::Build(constant: UserDetail::Contract)
  end
end
