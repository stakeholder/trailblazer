module  UserDetail::Operation
  # This is the operation for user detail resource show action
  class Show < Trailblazer::Operation
    include Helpers
    extend Representer::DSL

    representer :render, UserDetail::Representer

    step :model!, name: 'terminus.model.list'
    failure :log_error!, name: 'terminus.failure.log'

    def model!(options, *)
      options['model'] = UserDetail.find_by(user_id: options['params'][:id])
    end
  end
end
