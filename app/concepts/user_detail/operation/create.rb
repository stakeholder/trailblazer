module  UserDetail::Operation
  # This is the operation for user detail resource create action
  class Create < Trailblazer::Operation
    include Helpers
    extend Representer::DSL

    representer :render, UserDetail::Representer

    step     Model(UserDetail, :new), name: 'terminus.model.build'
    step     :attach_user!, name: 'terminus.model.setup'
    step     Contract::Build(constant: UserDetail::Contract),
             name: 'terminus.contract.build'
    step     Contract::Validate(), name: 'terminus.contract.validate'
    step     Contract::Persist(), name: 'terminus.contract.persist'
    failure  :log_error!, name: 'terminus.failure.log'

    def attach_user!(options, **)
      options['model'][:user_id] = options['params'][:id]
    end
  end
end
