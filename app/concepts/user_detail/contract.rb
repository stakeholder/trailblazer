# This is the contract class for user detail resource
class UserDetail::Contract < Reform::Form
  property :first_name
  property :last_name
  property :mobile_phone
  property :office_phone
  property :user_id

  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :user_id, presence: true, format: { with: /\d/ }
end
