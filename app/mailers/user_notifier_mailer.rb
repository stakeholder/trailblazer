# This is the signup email notifier
class UserNotifierMailer < ApplicationMailer
  default from: 'noreply@projectterminus.io'

  def send_signup_email(params)
    @params = params
    mail(to: @params[:email], subject: 'Welcome to Terminus')
  end
end
