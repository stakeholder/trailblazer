class CreateStates < ActiveRecord::Migration[5.1]
  def change
    create_table :states do |t|
      t.string :name
      t.string :abbreviation
      t.string :state_fips
      t.timestamps
    end
  end
end
