class CreateOriginator < ActiveRecord::Migration[5.1]
  def change
    create_table :originators do |t|
      t.string :legal_name
      t.string :display_name
      t.string :address
      t.string :address2
      t.string :city
      t.references :state, foreign_key: true
      t.string :zip_code
      t.boolean :is_deleted, default: false
      t.timestamps
    end
  end
end
