class CreateJoinTableUserGroup < ActiveRecord::Migration[5.1]
  def change
    create_join_table :users, :groups, table_name: :group_users  do |t|
      t.index :user_id
      t.index :group_id
    end
  end
end
