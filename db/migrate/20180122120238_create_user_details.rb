class CreateUserDetails < ActiveRecord::Migration[5.1]
  def change
    create_table :user_details do |t|
      t.references :user, foreign_key: true
      t.string :first_name
      t.string :last_name
      t.string :mobile_phone
      t.string :office_phone
      t.timestamps
    end
  end
end
