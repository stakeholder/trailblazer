class CreateJoinTableRolePermission < ActiveRecord::Migration[5.1]
  def change
    create_join_table :roles, :permissions, table_name: :permission_roles do |t|
      t.index :role_id
      t.index :permission_id
    end
  end
end
