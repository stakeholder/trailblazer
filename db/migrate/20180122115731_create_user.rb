class CreateUser < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :email
      t.string :password
      t.references :originator, foreign_key: true
      t.references :role, foreign_key: true
      t.boolean :is_deleted, default: false
      t.timestamps
    end
  end
end
