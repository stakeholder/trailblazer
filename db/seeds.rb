require 'faker'
require 'csv'

connection = ActiveRecord::Base.establish_connection.connection
truncate_tables_query = 'TRUNCATE states, originators, groups, roles, permissions, permission_roles, users, user_details RESTART IDENTITY CASCADE;'
connection.exec_query(truncate_tables_query)

# Seed State
state_csv_text = File.read(Rails.root.join("db", "state_data.csv"))
csv = CSV.parse(state_csv_text, :headers => true)
csv.each do |row|
  row_hash = row.to_hash
  State.create(
    name: row_hash["state_name"],
    abbreviation: row_hash["state_abbreviate"],
    state_fips: row_hash["state_fips"]
  )
end

# Seed Originators for terminus admin
Originator.create({
  legal_name: 'terminus',
  display_name: 'terminus',
  address: Faker::Address.street_name,
  address2: Faker::Address.street_address,
  city: Faker::Address.city,
  state: State.first,
  zip_code: Faker::Address.zip_code
})

# Seed two test originators
2.times do |n|
  Originator.create({
    legal_name: Faker::Name.unique.name,
    display_name: "Test Originator - #{n}",
    address: Faker::Address.street_name,
    address2: Faker::Address.street_address,
    city: Faker::Address.city,
    state: State.last,
    zip_code: Faker::Address.zip_code
  })
end

#Seed Groups
4.times do
  Group.create({
    name: Faker::Lorem.word,
    originator: Originator.last
  })
end

# Seed admin role
Role.create({
  name: 'Owner',
  description: 'Terminus administrator'
})


# Seed Roles for other users
['Admin', 'Regular'].each do |role_type|
  Role.create({
    name: role_type,
    description: "#{ role_type } user."
  })
end


#seed admin permissions

#assign admin role necessary permissions
# seed role for terminus admin
super_admin_permissions = [
  { name: "List permissions", url_regex: "/permissions", verb: "GET"},
  { name: "List originators", url_regex: "/originators", verb: "GET"},
  { name: "List groups", url_regex: "/groups", verb: "GET"},
  { name: "Create permissions", url_regex: "/permissions", verb: "POST"},
  { name: "Create groups", url_regex: "/groups", verb: "POST"},
  { name: "Create roles", url_regex: "/roles", verb: "POST"},
  { name: "Create originators", url_regex: "/originators", verb: "POST"},
]

admin_permissions = [
  { name: "List users", url_regex: "/users", verb: "GET"},
  { name: "List roles", url_regex: "/roles", verb: "GET"},
  { name: "Create auction", url_regex: "/auctions", verb: "POST"},
  { name: "Create users", url_regex: "/users", verb: "POST"},
  { name: "Create criterion", url_regex: "/criteria", verb: "POST"},
  { name: "Import county data", url_regex: "/custom-county-data", verb: "POST"},
  { name: "Import auction data", url_regex: "/custom-auction-data", verb: "POST"},
]

regular_user_permissions = [
  { name: "List states", url_regex: "/states", verb: "GET"},
  { name: "Filter by criteria", url_regex: "/filter-by-criteria", verb: "POST"},
  { name: "Filter by no-bid list", url_regex: "/filter-by-data-set", verb: "POST"},
  { name: "Fetch bidlist", url_regex: "/bidlist", verb: "GET"},
  { name: "Fetch auction ratings", url_regex: "/auction-ratings", verb: "GET"},
  { name: "Fetch MPC result", url_regex: "/mpc-result", verb: "GET"},
  { name: "List criteria", url_regex: "/criteria", verb: "GET"},
  { name: "List auctions", url_regex: "/auctions", verb: "GET"},
  { name: "Update user profile details", url_regex: "/users/[0-9]+/details", verb: "POST"},
  { name: "Get user profile details", url_regex: "/users/[0-9]+/details", verb: "GET"},
  { name: "List auctions properties", url_regex: "/auctions/[0-9]+/properties(\\?\\w=\\w)?", verb: "GET"},
  { name: "Update auction list", url_regex: "/auctions/[0-9]+/properties", verb: "PUT"},
  { name: "Upload auction list", url_regex: "/auctions/[0-9]+/properties", verb: "POST"},
  { name: "Fetch property details", url_regex: "/properties/[a-zA-Z_]+/[0-9]+", verb: "GET"},
  { name: "Search properties", url_regex: "/properties/search", verb: "GET"},
  { name: "Load states", url_regex: "/properties/states", verb: "GET"},
  { name: "Properties service EventSource tasks", url_regex: "/properties-tasks", verb: "GET"},
  { name: "Properties service EventSource tasks", url_regex: "/auctions-tasks", verb: "GET"},
  { name: "Event source tasks", url_regex: "/tasks", verb: "GET"},
  { name: "Fetch pre-bidlist", url_regex: "/pre-bidlist", verb: "GET"},
  { name: "Update auction ratings", url_regex: "/auction-ratings/[a-zA-Z0-9]+", verb: "PUT"},
  { name: "Change password", url_regex: "/users/change_password", verb: "POST"},
  { name: "Fetch strategy templates", url_regex: "/strategy-templates", verb: "GET"},
  { name: "Fetch strategy attributes", url_regex: "/strategy-attributes/*", verb: "GET"},
  { name: "Update strategy attributes", url_regex: "/strategy-attributes", verb: "PUT"},
  { name: "Save strategy", url_regex: "/strategies", verb: "POST"},
  { name: "Update strategy", url_regex: "/strategies", verb: "PUT"},
  { name: "Get strategy details", url_regex: "/strategies", verb: "GET"},
  { name: "Get risk rating logs", url_regex: "/risk-rating/logs*", verb: "GET"},
]

# create RolesPermissions entrie for super-admins
all_permissions = super_admin_permissions + admin_permissions + regular_user_permissions
all_permissions.each { |permission_record| Permission.create!(permission_record) }

# create RolesPermissions entries for admins
admin_role = Role.find_by(name: 'Admin')
persisted_admin_permissions = Permission.where(name: (admin_permissions + regular_user_permissions).map { |item| item[:name] })

persisted_admin_permissions.map do |permission|
  PermissionRole.create!(role: admin_role, permission: permission)
end

# create RolesPermissions entries for regular users
user_role = Role.find_by(name: 'Regular')
persisted_user_permissions = Permission.where(name: regular_user_permissions.map { |item| item[:name] })

persisted_user_permissions.map do |permission|
  PermissionRole.create!(role: user_role, permission: permission)
end

# Seed  terminus admin user
User.create({
  email: 'admin@projectterminus.io',
  password: "test1234",
  role: Role.terminus.first,
  originator: Originator.terminus.first
  })

Originator.default.each do |originator|
  User.create({
    email: Faker::Internet.email,
    password: "test1234",
    role: Role.find_by(name: 'Admin'),
    originator: originator
  })
  User.create({
    email: Faker::Internet.email,
    password: "test1234",
    role: Role.find_by(name: 'Regular'),
    originator: originator
  })
end

#Seed UserDetails
User.pluck(:id).each do |user_id|
  UserDetail.create({
    first_name: Faker::Name.first_name,
    last_name: Faker::Name.last_name,
    mobile_phone: Faker::PhoneNumber.phone_number,
    office_phone: Faker::PhoneNumber.phone_number,
    user_id: user_id
  })
end
