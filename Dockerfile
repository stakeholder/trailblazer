FROM ruby:2.5

MAINTAINER evan@pinevalleyre.com

# Set working directory
RUN mkdir -p /var/www
WORKDIR /var/www

# Install gems
RUN echo "Installing dependencies..."
ADD Gemfile /var/www/Gemfile
ADD Gemfile.lock /var/www/Gemfile.lock
RUN bundle install
RUN echo "Installing figaro....."
RUN bundle exec figaro install

# Stop process running on port
RUN rm -f /var/www/tmp/pids/server.pid

# Copy files into working directory
ADD . /var/www

CMD ["bash", "/var/www/server.sh"]
