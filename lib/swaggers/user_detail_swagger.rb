module Swagger::UserDetail
  module Model
    def load_swagger
      swagger_schema :UserDetail do
        key :required, %i[user_id]

        property :id do
          key :type, :integer
        end
        
        property :first_name do
          key :type, :string
        end

        property :last_name do
          key :type, :string
        end

        property :mobile_phone do
          key :type, :string
        end

        property :office_phone do
          key :type, :string
        end

        property :user_id do
          key :type, :integer
        end
      end
    end
  end
end
