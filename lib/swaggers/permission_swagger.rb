module Swagger::Permission
  module Controller
    def load_swagger
    swagger_path '/permissions' do
      operation :get do
        key :summary, 'All Permissions'
        key :description, '**Returns all permissions from the system**'
        key :operationId, 'get_permissions'
        key :produces, [
          'application/json'
        ]
        key :tags, [
          'permission'
        ]
        parameter do
          key :name, :permissions
          key :in,   :path
          key :description, 'Fetches all the permissions in the system'
          key :required, true
          key :type, :string
        end
        response 200 do
          key :description, 'an array of permissions will be returned'
          schema do
            key :type, :array
            items do
              key :'$ref', :Permission
            end
          end
        end
        response 400 do
          key :description, 'Bad Request'
        end
        response 500 do
          key :description, 'Unexpected Error'
        end
      end
      operation :post do
        key :summary, 'Adds a new permission'
        key :description, '**Creates a new permission in the system. Duplicates are not allowed**'
        key :operationId, 'addPermission'
        key :produces, [
          'application/json'
        ]
        key :tags, [
          'permission'
        ]
        parameter do
          key :name,  :role
          key :in,  :body
          key :description, 'Permission to add to system'
          key :required,  true
          schema do
            key :'$ref',  :PermissionInput
          end
        end
        response 200 do
          key :description, 'successfull operation'
          schema do
            key :'$ref', :Permission
          end
        end
        response 400 do
          key :description, 'Bad Request'
        end
        response 500 do
          key :description, 'Unexpected Error'
        end
      end
    end

    swagger_path '/permissions/{id}' do
      parameter do
        key :name, :id
        key :in, :path
        key :description, 'ID of permission to fetch'
        key :required, true
        key :type, :integer
        key :format, :int64
      end
      operation :get  do
        key :summary, 'Find Permission by ID'
        key :description, '**Returns a single permission resource**'
        key :operationId, 'findPermissionById'
        key :tags, [
           'permission'
        ]
        response 200 do
          key :description, 'returns a role resource'
          schema do
            key :'$ref', :Permission
          end
        end
        response 400 do
          key :description, 'bad request'
        end
        response 404 do
          key :description, 'Permission resource doesn\'t exist'
        end
        response 500 do
          key :description, 'Unexpected Error'
        end
      end

      operation :put  do
        key :summary, 'Update Permission by ID'
        key :description, '**Returns an updated permission resource**'
        key :operationId, 'updatePermission'
        key :produces, [
          'application/json'
        ]
        key :tags, [
           'permission'
        ]
        parameter do
          key :name,  :permission
          key :in,  :body
          key :description, 'Permission to add to system'
          key :required,  true
          schema do
            key :'$ref',  :PermissionInput
          end
        end
        response 200 do
          key :description, 'returns a permission resource'
          schema do
            key :'$ref', :Permission
          end
        end
        response 400 do
          key :description, 'Bad Request'
        end
        response 404 do
          key :description, 'Permission resourcce doesn\'t exist'
        end
        response 500 do
          key :description, 'Unexpected Error'
        end
      end
    end
  end
  end

  module Model
    def load_swagger_schema
    swagger_schema :Permission do
      key :required, %i[id name description]
      property :id do
        key :type, :integer
        key :format, :int64
      end
      property :name do
        key :type, :string
      end
      property :description do
        key :type, :string
      end
      property :created_at do
        key :type, :string
        key :format, :date_time
      end
      property :updated_at do
        key :type, :string
        key :format, :date_time
      end
    end
  end

    def load_swagger_input_schema
    swagger_schema :PermissionInput do
      allOf do
        schema do
          key :required, %i[name description]
          property :name do
            key :type, :string
          end
          property :description do
            key :type, :string
          end
          property :created_at do
            key :type, :string
            key :format, :date_time
          end
          property :updated_at do
            key :type, :string
            key :format, :date_time
          end
        end
      end
    end
  end
end

end
