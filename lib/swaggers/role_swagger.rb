module Swagger::Role
  module Controller
    def load_swagger
      swagger_path '/roles' do
        operation :get do
          key :summary, 'Gets all roles'
          key :description, '**Returns all roles from the system**'
          key :operationId, 'getRoles'
          key :produces, [
            'application/json'
          ]
          key :tags, [
            'role'
          ]
          parameter do
            key :name, :roles
            key :in,   :path
            key :description, 'Fetches all the roles in the system'
            key :required, true
            key :type, :string
          end
          response 200 do
            key :description, 'an array of roles will be returned'
            schema do
              key :type, :array
              items do
                key :'$ref', :Role
              end
            end
          end
          response 400 do
            key :description, 'Bad Request'
          end
          response 500 do
            key :description, 'Unexpected Error'
          end
        end
        operation :post do
          key :summary, 'Adds a new role'
          key :description, '**Creates a new role in the system. Duplicates are not allowed**'
          key :operationId, 'addRole'
          key :produces, [
            'application/json'
          ]
          key :tags, [
            'role'
          ]
          parameter do
            key :name,  :role
            key :in,  :body
            key :description, 'Role to add to system'
            key :required,  true
            schema do
              key :'$ref',  :RoleInput
            end
          end
          response 200 do
            key :description, 'successfull operation'
            schema do
              key :'$ref', :Role
            end
          end
          response 400 do
            key :description, 'Bad Request'
          end
          response 500 do
            key :description, 'Unexpected Error'
          end
        end
      end

      swagger_path '/roles/{id}' do
        parameter do
          key :name, :id
          key :in, :path
          key :description, 'ID of role to fetch'
          key :required, true
          key :type, :integer
          key :format, :int64
        end
        operation :get  do
          key :summary, 'Find Role by ID'
          key :description, '**Returns a single role resource**'
          key :operationId, 'findRoleById'
          key :produces, [
            'application/json'
          ]
          key :tags, [
             'role'
          ]
          response 200 do
            key :description, 'returns a role resource'
            schema do
              key :'$ref', :Role
            end
          end
          response 400 do
            key :description, 'Bad Request'
          end
          response 404 do
            key :description, 'Role resourcce doesn\'t exist'
          end
          response 500 do
            key :description, 'Unexpected Error'
          end
        end

        operation :put  do
          key :summary, 'Update Role by ID'
          key :description, '**Returns an updated role resource**'
          key :operationId, 'updateRole'
          key :produces, [
            'application/json'
          ]
          key :tags, [
             'role'
          ]
          parameter do
            key :name,  :role
            key :in,  :body
            key :description, 'Role to add to system'
            key :required,  true
            schema do
              key :'$ref',  :RoleInput
            end
          end
          response 200 do
            key :description, 'returns a role resource'
            schema do
              key :'$ref', :Role
            end
          end
          response 400 do
            key :description, 'Bad Request'
          end
          response 404 do
            key :description, 'Role resourcce doesn\'t exist'
          end
          response 500 do
            key :description, 'Unexpected Error'
          end
        end
      end
    end
  end

  module Model
    def load_swagger_schema
      swagger_schema :Role do
        key :required, %i[id name description]
        property :id do
          key :type, :integer
          key :format, :int64
        end
        property :name do
          key :type, :string
        end
        property :description do
          key :type, :string
        end
        property :created_at do
          key :type, :string
          key :format, :date_time
        end
        property :updated_at do
          key :type, :string
          key :format, :date_time
        end
      end
    end

    def load_swagger_input_schema
      swagger_schema :RoleInput do
        allOf do
          schema do
            key :required, %i[name description]
            property :name do
              key :type, :string
            end
            property :description do
              key :type, :string
            end
            property :created_at do
              key :type, :string
              key :format, :date_time
            end
            property :updated_at do
              key :type, :string
              key :format, :date_time
            end
          end
        end
      end
    end
  end
end
