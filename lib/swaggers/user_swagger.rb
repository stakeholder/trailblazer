module Swagger::User
  module Controller
    def load_swagger
      swagger_path '/users' do
        operation :get do
          key :summary, 'Fetch users'
          key :description, 'Returns all users from the system that' \
          ' the user has access to'
          key :operationId, 'fetch_users'
          key :produces, [
            'application/json'
          ]
          key :tags, [
            'user'
          ]

          response 200 do
            key :description, 'Success'
            schema do
              key :type, :array
              items do
                key :'$ref', :User
              end
            end
          end

          response 500 do
            key :message, 'Error'
            key :description, 'Soemthing went wrong'
          end
        end

        operation :post do
          key :summary, 'Add user'
          key :description, 'Creates a new user, unique by email'
          key :operationId, 'add_user'
          key :produces, [
            'application/json'
          ]
          key :tags, [
            'user'
          ]

          parameter do
            key :name, :user
            key :in, :body
            key :description, 'User to add to the database'
            key :required, true
            schema do
              key :'$ref', :User
            end
          end

          response 200 do
            key :description, 'Success response'
            schema do
              key :'$ref', :User
            end
          end

          response 400 do
            key :message, 'Invalid user data supplied'
          end

          response 500 do
            key :message, 'Error'
            key :description, 'Soemthing went wrong'
          end
        end
      end

      swagger_path '/users/{id}' do
        operation :get do
          key :summary, 'Find user by ID'
          key :notes, 'Returns a user based on ID'
          key :produces, [
            'application/json'
          ]
          key :tags, [
            'user'
          ]
          parameter do
            key :name, :id
            key :in, :path
            key :description, 'ID of user to be fetched'
            key :required, true
            key :type, :integer
          end

          response 200 do
            key :description, 'Success response'
            schema do
              key :'$ref', :User
            end
          end

          response 400 do
            key :description, 'Invalid ID supplied'
          end

          response 404 do
            key :description, 'User not found'
          end

          response 500 do
            key :message, 'Error'
            key :description, 'Soemthing went wrong'
          end
        end

        operation :put do
          key :summary, 'Update user by ID'
          key :notes, 'Updates a user based on ID'
          key :produces, [
            'application/json'
          ]
          key :tags, [
            'user'
          ]
          parameter do
            key :name, :id
            key :in, :path
            key :description, 'ID of user to be updated'
            key :required, true
            key :type, :integer
          end

          parameter do
            key :name, :user
            key :in, :body
            key :description, 'User data to be updated'
            schema do
              key :'$ref', :User
            end
          end

          response 200 do
            key :description, 'Success response'
            schema do
              key :'$ref', :User
            end
          end

          response 400 do
            key :description, 'Invalid ID supplied'
          end

          response 404 do
            key :description, 'User not found'
          end

          response 500 do
            key :message, 'Error'
            key :description, 'Soemthing went wrong'
          end
        end

        operation :delete do
          key :summary, 'Delete user by ID'
          key :notes, 'Deletes a user based on ID'
          key :produces, [
            'application/json'
          ]
          key :tags, [
            'user'
          ]
          parameter do
            key :name, :id
            key :in, :path
            key :description, 'ID of user to be deleted'
            key :required, true
            key :type, :integer
          end

          response 204 do
            key :description, 'Success'
          end

          response 404 do
            key :description, 'User not found'
          end

          response 500 do
            key :message, 'Error'
            key :description, 'Soemthing went wrong'
          end
        end
      end

      swagger_path '/users/{id}/details' do
        operation :get do
          key :summary, 'Show user details'
          key :notes, 'Returns a user detalil record based on user id'
          key :produces, [
            'application/json'
          ]
          key :tags, [
            'user'
          ]
          parameter do
            key :name, :id
            key :in, :path
            key :description, 'ID of user whose details are to be fetched'
            key :required, true
            key :type, :integer
          end

          response 200 do
            key :description, 'Success response'
            schema do
              key :'$ref', :UserDetail
            end
          end

          response 400 do
            key :description, 'Invalid ID supplied'
          end

          response 404 do
            key :description, 'User not found'
          end

          response 500 do
            key :message, 'Error'
            key :description, 'Soemthing went wrong'
          end
        end

        operation :put do
          key :summary, 'Update user details'
          key :notes, 'Updates a user\'s details based on user id'
          key :produces, [
            'application/json'
          ]
          key :tags, [
            'user'
          ]

          parameter do
            key :name, :id
            key :in, :path
            key :description, 'ID of user whose details are to be updated'
            key :required, true
            key :type, :integer
          end

          parameter do
            key :name, :user_detail
            key :in, :body
            key :description, 'User details to add to update'
            schema do
              key :'$ref', :UserDetail
            end
          end

          response 200 do
            key :description, 'Success response'
            schema do
              key :'$ref', :UserDetail
            end
          end

          response 400 do
            key :description, 'Invalid ID supplied'
          end

          response 404 do
            key :message, 'User not found'
          end

          response 500 do
            key :message, 'Error'
            key :description, 'Soemthing went wrong'
          end
        end

        operation :post do
          key :summary, 'Add user details'
          key :notes, 'Adds a user\'s details based on user id'
          key :produces, [
            'application/json'
          ]
          key :tags, [
            'user'
          ]

          parameter do
            key :name, :user_detail
            key :in, :body
            key :description, 'User details to add to the database'
            key :required, true
            schema do
              key :'$ref', :UserDetail
            end
          end

          response 200 do
            key :description, 'Success response'
            schema do
              key :'$ref', :UserDetail
            end
          end

          response 400 do
            key :description, 'Invalid data supplied'
          end

          response 404 do
            key :description, 'User not found'
          end

          response 500 do
            key :message, 'Error'
            key :description, 'Soemthing went wrong'
          end
        end


        operation :post do
          key :summary, 'Update user password'
          key :notes, "Updates a user's password"
          key :produces, [
            'application/json'
          ]
          key :tags, [
            'user'
          ]
          parameter do
            key :name, :id
            key :in, :body
            key :description, 'ID of user to be updated'
            key :required, true
            key :type, :integer
          end

          parameter do
            key :name, :current_password
            key :in, :body
            key :description, 'Password to be replaced'
            key :required, true
            key :type, :string
          end

          parameter do
            key :name, :new_password
            key :in, :body
            key :description, 'Password to be used as replacement'
            key :required, true
            key :type, :string
          end

          response 200 do
            key :description, 'Success response'
            schema do
              key :'$ref', :User
            end
          end

          response 400 do
            key :description, 'Invalid data supplied'
          end

          response 404 do
            key :description, 'User not found'
          end

          response 500 do
            key :message, 'Error'
            key :description, 'Soemthing went wrong'
          end
        end

      end
    end
  end

  module Model
    def load_swagger
      swagger_schema :User do
        key :required, %i[email role_id vendor_id originator_id]
        property :email do
          key :type, :string
        end

        property :id do
          key :type, :integer
        end

        property :role_id do
          key :type, :string
        end

        property :vendor_id do
          key :type, :string
        end

        property :originator_id do
          key :type, :string
        end

        property :user_detail do
          key :type, :object do
            schema do
              key :'$ref', :UserDetail
            end
          end
        end
      end
    end
  end
end
