module Swagger::Originator
  module Controller
    def load_swagger
      swagger_path '/originators' do
        operation :get do
          key :summary, 'Fetch originators'
          key :description, 'Returns all originators'
          key :operationId, 'fetch_originators'
          key :produces, [
            'application/json'
          ]
          key :tags, [
            'originator'
          ]

          response 200 do
            key :description, 'Success'
            schema do
              key :type, :array
              items do
                key :'$ref', :Originator
              end
            end
          end

          response 400 do
            key :message, 'Invalid ID supplied'
          end

          response 404 do
            key :message, 'Originator not found'
          end

          response 500 do
            key :description, 'Error'
            key :message, 'Soemthing went wrong'
          end
        end

        operation :post do
          key :summary, 'Add originator'
          key :description, 'Creates a new originator'
          key :operationId, 'add_originator'
          key :produces, [
            'application/json'
          ]

          key :tags, [
            'originator'
          ]

          parameter do
            key :name, :originator
            key :in, :body
            key :description, 'originator to add'
            key :required, true
            schema do
              key :'$ref', :Originator
            end
          end

          response 200 do
            key :description, 'Success'
            schema do
              key :'$ref', :Originator
            end
          end

          response 400 do
            key :message, 'Invalid ID supplied'
          end

          response 404 do
            key :message, 'Originator not found'
          end

          response 500 do
            key :message, 'Error'
            key :description, 'Soemthing went wrong'
          end
        end
      end

      swagger_path '/originators/{id}' do
        operation :get do
          key :summary, 'Find originator by ID'
          key :notes, 'Returns a originator based on ID'
          key :produces, [
            'application/json'
          ]
          key :tags, [
            'originator'
          ]
          parameter do
            key :name, :id
            key :in, :path
            key :description, 'ID of originator to be fetched'
            key :required, true
            key :type, :integer
          end

          response 200 do
            key :message, 'Success'
            key :description, 'originator response'
            schema do
              key :'$ref', :Originator
            end
          end

          response 400 do
            key :description, 'Invalid ID supplied'
          end

          response 404 do
            key :description, 'Originator not found'
          end

          response 500 do
            key :message, 'Error'
            key :description, 'Soemthing went wrong'
          end
        end

        operation :put do
          key :summary, 'Update originator by ID'
          key :notes, 'Updates a originator based on ID'
          key :produces, [
            'application/json'
          ]
          key :tags, [
            'originator'
          ]
          parameter do
            key :name, :id
            key :in, :path
            key :description, 'ID of originator to be updated'
            key :required, true
            key :type, :integer
          end

          parameter do
            key :name, :originator
            key :in, :body
            key :description, 'originator to add'
            schema do
              key :'$ref', :Originator
            end
          end

          response 200 do
            key :message, 'Success'
            key :description, 'originator response'
            schema do
              key :'$ref', :Originator
            end
          end

          response 400 do
            key :description, 'Invalid ID supplied'
          end

          response 404 do
            key :description, 'Originator not found'
          end

          response 500 do
            key :message, 'Error'
            key :description, 'Soemthing went wrong'
          end
        end

        operation :delete do
          key :summary, 'Delete originator by ID'
          key :notes, 'Deletes a originator based on ID'
          key :produces, [
            'application/json'
          ]
          key :tags, [
            'originator'
          ]
          parameter do
            key :name, :id
            key :in, :path
            key :description, 'ID of originator to be deleted'
            key :required, true
            key :type, :integer
          end

          response 204 do
            key :description, 'Success'
          end

          response 404 do
            key :description, 'Originator not found'
          end

          response 500 do
            key :message, 'Error'
            key :description, 'Soemthing went wrong'
          end
        end
      end
    end
  end

  module Model
    def load_swagger
      swagger_schema :Originator do
        key :required, %i[legal_name display_name address state zip_code]

        property :id do
          key :type, :integer
        end

        property :legal_name do
          key :type, :string
        end

        property :display_name do
          key :type, :string
        end

        property :vendor_id do
          key :type, :string
        end

        property :address do
          key :type, :string
        end

        property :address2 do
          key :type, :string
        end

        property :city do
          key :type, :string
        end

        property :state do
          key :type, :string
        end

        property :zip_code do
          key :type, :string
        end
      end
    end
  end
end
