module Swagger::Group
  module Controller
    def load_swagger
      swagger_path '/groups' do
        operation :get do
          key :summary, 'Fetch groups'
          key :description, 'Returns all groups'
          key :operationId, 'fetch_groups'
          key :produces, [
            'application/json'
          ]
          key :tags, [
            'group'
          ]

          response 200 do
            key :description, 'Success'
            schema do
              key :type, :array
              items do
                key :'$ref', :Group
              end
            end
          end

          response 404 do
            key :message, 'Group not found'
          end

          response 500 do
            key :message, 'Error'
            key :description, 'Soemthing went wrong'
          end
        end

        operation :post do
          key :summary, 'Add group'
          key :description, 'Creates a new group'
          key :operationId, 'add_group'
          key :produces, [
            'application/json'
          ]
          key :tags, [
            'group'
          ]

          parameter do
            key :name, :group
            key :in, :body
            key :description, 'group to add'
            key :required, true
            schema do
              key :'$ref', :Group
            end
          end

          response 200 do
            key :description, 'group response'
            schema do
              key :'$ref', :Group
            end
          end

          response 404 do
            key :description, 'Group not found'
          end

          response 500 do
            key :message, 'Error'
            key :description, 'Soemthing went wrong'
          end
        end
      end

      swagger_path '/groups/{id}' do
        operation :get do
          key :summary, 'Find group by ID'
          key :notes, 'Returns a group based on ID'
          key :produces, [
            'application/json'
          ]
          key :tags, [
            'group'
          ]
          parameter do
            key :name, :id
            key :in, :path
            key :description, 'ID of group to be fetched'
            key :required, true
            key :type, :integer
          end

          response 200 do
            key :message, 'Success'
            key :description, 'group response'
            schema do
              key :'$ref', :Group
            end
          end

          response 400 do
            key :description, 'Invalid ID supplied'
          end

          response 500 do
            key :message, 'Error'
            key :description, 'Soemthing went wrong'
          end
        end

        operation :put do
          key :summary, 'Update group by ID'
          key :notes, 'Updates a group based on ID'
          key :produces, [
            'application/json'
          ]
          key :tags, [
            'group'
          ]

          parameter do
            key :name, :id
            key :in, :path
            key :description, 'ID of group to be updated'
            key :required, true
            key :type, :integer
          end

          parameter do
            key :name, :group
            key :in, :body
            key :description, 'group data to update'
            schema do
              key :'$ref', :Group
            end
          end

          response 200 do
            key :message, 'Success'
            key :description, 'group response'
            schema do
              key :'$ref', :Group
            end
          end

          response 400 do
            key :description, 'Invalid ID supplied'
          end

          response 404 do
            key :description, 'Group not found'
          end

          response 500 do
            key :message, 'Error'
            key :description, 'Soemthing went wrong'
          end
        end

        operation :delete do
          key :summary, 'Delete group by ID'
          key :notes, 'Deletes a group based on ID'
          key :produces, [
            'application/json'
          ]
          key :tags, [
            'group'
          ]
          parameter do
            key :name, :id
            key :in, :path
            key :description, 'ID of group to be deleted'
            key :required, true
            key :type, :integer
          end

          response 204 do
            key :description, 'Success response'
          end

          response 404 do
            key :description, 'Group not found'
          end

          response 500 do
            key :message, 'Error'
            key :description, 'Soemthing went wrong'
          end
        end
      end
    end
  end

  module Model
    def load_swagger
      swagger_schema :Group do
        key :required, %i[name originator_id]
        property :id do
          key :type, :integer
        end

        property :name do
          key :type, :string
        end

        property :originator_id do
          key :type, :string
        end
      end
    end
  end
end
